package com.totalbp.mmtransfer.model;

/**
 * Created by Ezra
 */

public class ListItemIdleEnt {

    public ListItemIdleEnt() {
    }

    public String getKd_Anak_Kd_Material() {
        return Kd_Anak_Kd_Material;
    }

    public void setKd_Anak_Kd_Material(String kd_Anak_Kd_Material) {
        Kd_Anak_Kd_Material = kd_Anak_Kd_Material;
    }

    public String getVol() {
        return vol;
    }

    public void setVol(String vol) {
        this.vol = vol;
    }

    public String getDeskripsi() {
        return Deskripsi;
    }

    public void setDeskripsi(String deskripsi) {
        Deskripsi = deskripsi;
    }

    public ListItemIdleEnt(String kd_Anak_Kd_Material, String vol, String deskripsi) {

        Kd_Anak_Kd_Material = kd_Anak_Kd_Material;
        this.vol = vol;
        Deskripsi = deskripsi;
    }

    public String Kd_Anak_Kd_Material;
    public String vol;
    public String Deskripsi;

}

