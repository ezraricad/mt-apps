package com.totalbp.mmtransfer.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by Ezra.R on 19/10/2017.
 */

public class UserPrivilegeEnt implements Serializable {
    @SerializedName("UserID")
    String UserID;
    @SerializedName("KodeProyek")
    String KodeProyek;
    @SerializedName("App_ID")
    String App_ID;
    @SerializedName("Form_ID")
    String Form_ID;
    @SerializedName("form_name")
    String form_name;

    @SerializedName("To_Approve")
    String To_Approve;
    @SerializedName("To_Delete")
    String To_Delete;
    @SerializedName("To_Edit")
    String To_Edit;
    @SerializedName("To_Insert")
    String To_Insert;
    @SerializedName("To_Print")
    String To_Print;
    @SerializedName("To_View")
    String To_View;

    @SerializedName("To_Quality")
    String To_Quality;
    @SerializedName("To_HSE")
    String To_HSE;

    public UserPrivilegeEnt(String to_Approve, String to_Delete, String to_Edit, String to_Insert, String to_Print, String to_View, String to_Quality, String to_HSE) {
        To_Approve = to_Approve;
        To_Delete = to_Delete;
        To_Edit = to_Edit;
        To_Insert = to_Insert;
        To_Print = to_Print;
        To_View = to_View;
        To_View = to_Quality;
        To_View = to_HSE;
    }

    public UserPrivilegeEnt() {
    }

    public String getUserID() {
        return UserID;
    }

    public String getTo_Quality() {
        return To_Quality;
    }

    public void setTo_Quality(String to_Quality) {
        To_Quality = to_Quality;
    }

    public String getTo_HSE() {
        return To_HSE;
    }

    public void setTo_HSE(String to_HSE) {
        To_HSE = to_HSE;
    }

    public void setUserID(String userID) {
        UserID = userID;
    }

    public String getKodeProyek() {
        return KodeProyek;
    }

    public void setKodeProyek(String kodeProyek) {
        KodeProyek = kodeProyek;
    }

    public String getApp_ID() {
        return App_ID;
    }

    public void setApp_ID(String app_ID) {
        App_ID = app_ID;
    }

    public String getForm_ID() {
        return Form_ID;
    }

    public void setForm_ID(String form_ID) {
        Form_ID = form_ID;
    }

    public String getForm_name() {
        return form_name;
    }

    public void setForm_name(String form_name) {
        this.form_name = form_name;
    }

    public String getTo_Approve() {
        return To_Approve;
    }

    public void setTo_Approve(String to_Approve) {
        To_Approve = to_Approve;
    }

    public String getTo_Delete() {
        return To_Delete;
    }

    public void setTo_Delete(String to_Delete) {
        To_Delete = to_Delete;
    }

    public String getTo_Edit() {
        return To_Edit;
    }

    public void setTo_Edit(String to_Edit) {
        To_Edit = to_Edit;
    }

    public String getTo_Insert() {
        return To_Insert;
    }

    public void setTo_Insert(String to_Insert) {
        To_Insert = to_Insert;
    }

    public String getTo_Print() {
        return To_Print;
    }

    public void setTo_Print(String to_Print) {
        To_Print = to_Print;
    }

    public String getTo_View() {
        return To_View;
    }

    public void setTo_View(String to_View) {
        To_View = to_View;
    }
}
