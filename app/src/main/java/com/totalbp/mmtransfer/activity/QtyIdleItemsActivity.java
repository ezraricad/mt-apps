package com.totalbp.mmtransfer.activity;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.DividerItemDecoration;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.TextView;
import android.widget.Toast;

import com.totalbp.mmtransfer.R;
import com.totalbp.mmtransfer.adapter.ListIdleItemAdapter;
import com.totalbp.mmtransfer.adapter.ListRequestItemAdapter;
import com.totalbp.mmtransfer.config.SessionManager;
import com.totalbp.mmtransfer.controller.MMController;
import com.totalbp.mmtransfer.interfaces.VolleyCallback;
import com.totalbp.mmtransfer.model.ListItemIdleEnt;
import com.totalbp.mmtransfer.model.ListItemRequestOut;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import static android.nfc.tech.MifareUltralight.PAGE_SIZE;

/**
 * Created by Ezra.R on 09/10/2017.
 */

public class QtyIdleItemsActivity extends AppCompatActivity implements SwipeRefreshLayout.OnRefreshListener, ListIdleItemAdapter.MessageAdapterListener
{
    private SessionManager session;
    private MMController controller;
    private RecyclerView recyclerView;
    private SwipeRefreshLayout swipeRefreshLayout;
    public ListIdleItemAdapter adapter;
    public List<ListItemIdleEnt> listItems = new ArrayList<>();

    private ActionModeCallback actionModeCallback;
    private ActionMode actionMode;
    JSONObject item;

    private Boolean isFabOpen = false;
    private FloatingActionButton fab,fab1,fab2;
    private Animation fab_open,fab_close,rotate_forward,rotate_backward;
    private TextView tvLabelNewMo, tvLabelFiler;

    private static final int request_data_from  = 20;
    private static final int request_data_from_2  = 21;

    private static final int request_data_to_confirmation  = 22;

    public Integer currentPage = 1;
    public Integer pageSize = 10;

    public Integer searchActive = 0;

    public QtyIdleItemsActivity() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_idlematerial);

        controller = new MMController();
        session = new SessionManager(getApplicationContext());

        getSupportActionBar().setTitle("List Item Idle");

        //Start Recycler View
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        adapter = new ListIdleItemAdapter(this, listItems, this);
        final LinearLayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        mLayoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setHasFixedSize(true);
        recyclerView.addItemDecoration(new DividerItemDecoration(this, LinearLayoutManager.VERTICAL));
        recyclerView.setAdapter(adapter);
        actionModeCallback = new ActionModeCallback();
        swipeRefreshLayout.post(
                new Runnable() {
                    @Override
                    public void run() {
                        if(isNetworkAvailable()){
                            SetListView("", currentPage.toString(), pageSize.toString());
                        }
                        else
                        {
                        }
                    }
                }
        );


        recyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);

            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                //JIKA BUKAN HASIL SEARCH MAKA ADD SCROOL AKTIF
                if(!searchActive.equals("1")){
                    int visibleItemCount = mLayoutManager.getChildCount();
                    int totalItemCount = mLayoutManager.getItemCount();
                    int firstVisibleItemPosition = mLayoutManager.findFirstVisibleItemPosition();
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0
                            && totalItemCount >= PAGE_SIZE) {

                        currentPage = currentPage + 1;
                        SetListView("", currentPage.toString(), pageSize.toString());
                        Log.d("onScroll", currentPage.toString());

                    }
                }
                else
                {

                }
            }
        });
    }



    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);

        if( requestCode == request_data_from ) {
            if (data != null){
                //projectSelected.setText(data.getExtras().getString("projectname"));
                //session.setKodeProyek(data.getExtras().getString("kodeProyek"));
                //session.setNamaProyek(data.getExtras().getString("projectname"));
                listItems.clear();
                SetListView("", "1", "10");
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.search_item, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_search) {

            Intent intent2=new Intent(QtyIdleItemsActivity.this,RequestMaterialFilterActivity.class);
            intent2.putExtra("fromrequestmaterial","true");
            startActivityForResult(intent2, request_data_from_2);

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void SetListView(String wherecond, String currentpage, String pagesize){
        swipeRefreshLayout.setRefreshing(true);
        session = new SessionManager(getApplicationContext());
        //listItems.clear();
        String whereCond = "";
        if (!wherecond.equals("")){
            whereCond = wherecond;
        }
        controller.InqGeneral2(getApplicationContext(),"GetItemQtyIdleStock",
                "@KodeProject",session.getKodeProyek(),
                "@currentpage",currentpage,
                "@pagesize",pagesize,
                "@sortby","",
                "@wherecond"," WHERE ttrans.Kode_Proyek = '"+session.getKodeProyek()+"' AND ttrans.FlagReturnGudang = 1 ",
                "@nik",String.valueOf(session.isNikUserLoggedIn()),
                "@formid","MAMAN.02",
                "@zonaid","",
                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            Log.d("EZRA", result.toString());
                            swipeRefreshLayout.setRefreshing(false);
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    item = result.getJSONObject(i);
                                    ListItemIdleEnt listProjectEnt = new ListItemIdleEnt(
                                            item.getString("Kd_Anak_Kd_Material"), item.getString("Vol"),
                                            item.getString("Deskripsi")
                                    );

                                    listItems.add(listProjectEnt);
                                }
                                adapter.notifyDataSetChanged();
                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH "+e.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;
        if (networkInfo != null && networkInfo.isConnected()) {
            // Network is present and connected
            isAvailable = true;
            //btnConnect.setVisibility(View.GONE);
            //h2.removeCallbacks(run);
        }
        else
        {
            isAvailable = false;
            ///h2.postDelayed(run, 0);
            //btnConnect.setVisibility(View.VISIBLE);
        }
        return isAvailable;
    }



    private class ActionModeCallback implements ActionMode.Callback {
        @Override
        public boolean onCreateActionMode(ActionMode mode, Menu menu) {
            //mode.getMenuInflater().inflate(R.menu.detilpo_item, menu);

            // disable swipe refresh if action mode is enabled
            swipeRefreshLayout.setEnabled(false);
            return true;
        }

        @Override
        public boolean onPrepareActionMode(ActionMode mode, Menu menu) {
            return false;
        }

        @Override
        public boolean onActionItemClicked(ActionMode mode, MenuItem item) {
            switch (item.getItemId()) {
                case R.id.action_filter:
                    // delete all the selected messages
                    //deleteMessages();
                    mode.finish();
                    return true;

                default:
                    return false;
            }
        }

        @Override
        public void onDestroyActionMode(ActionMode mode) {
            //adapter.clearSelections();
            swipeRefreshLayout.setEnabled(true);
            actionMode = null;
            recyclerView.post(new Runnable() {
                @Override
                public void run() {
                    adapter.resetAnimationIndex();
                    // mAdapter.notifyDataSetChanged();
                }
            });
        }
    }

    private void enableActionMode(int position) {
        if (actionMode == null) {
            //actionMode = getActivity().startSupportActionMode(actionModeCallback);
            actionMode = ((AppCompatActivity) getApplicationContext()).startSupportActionMode(actionModeCallback);
        }
        toggleSelection(position);
    }

    private void toggleSelection(int position) {
        adapter.toggleSelection(position);
        int count = adapter.getSelectedItemCount();

        if (count == 0) {
            actionMode.finish();
        } else {
            actionMode.setTitle(String.valueOf(count));
            actionMode.invalidate();
        }
    }

    @Override
    public void onRefresh() {
        if(isNetworkAvailable()){
            listItems.clear();
            adapter.notifyDataSetChanged();
            currentPage = 1;
            SetListView("", "1", "10");
        }
        else
        {

        }
    }

    @Override
    public void onIconClicked(int position) {

    }

    @Override
    public void onIconImportantClicked(int position) {

    }

    @Override
    public void onMessageRowClicked(int position) {
        if (adapter.getSelectedItemCount() > 0) {
            enableActionMode(position);
        } else {
            // read the message which removes bold from the row
            ListItemIdleEnt message = listItems.get(position);
            //message.setRead(true);
            listItems.set(position, message);
            adapter.notifyDataSetChanged();


//                Log.d("dadapaha", message.getID().toString());
//                Intent intent = new Intent(this, ApprovalMaterialActivity.class);
//                intent.putExtra("IdMo", message.getID().toString());
//                intent.putExtra("KodeMo", message.getKode_MO().toString());
//                intent.putExtra("IdMaterial", message.getKode_Item().toString());
//                startActivityForResult(intent, request_data_to_confirmation);
        }

    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        this.finish();
    }

    @Override
    public void onRowLongClicked(int position) {

    }

}
