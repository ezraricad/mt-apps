package com.totalbp.mmtransfer.activity;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ActionMode;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.totalbp.mmtransfer.R;
import com.totalbp.mmtransfer.config.SessionManager;
import com.totalbp.mmtransfer.controller.MMController;
import com.totalbp.mmtransfer.interfaces.VolleyCallback;
import com.totalbp.mmtransfer.model.ApprovalEnt;
import com.totalbp.mmtransfer.model.CommonRequestMoFormEnt;
import com.totalbp.mmtransfer.model.ListItemRequestOutFullTable;
import com.totalbp.mmtransfer.model.ListItemTransferOutFullTableEnt;
import com.totalbp.mmtransfer.model.checkApprovalEnt;
import com.totalbp.mmtransfer.utils.MProgressDialog;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by Ezra.R on 09/10/2017.
 */

public class ApprovalTransferMaterialActivity extends AppCompatActivity
{
    private SessionManager session;
    private MMController controller;
    public List<ListItemRequestOutFullTable> listItems = new ArrayList<>();

    private ActionMode actionMode;
    JSONObject item;

    private Boolean isFabOpen = false;
    private FloatingActionButton fab,fab1,fab2;
    private Animation fab_open,fab_close,rotate_forward,rotate_backward;
    private TextView tvLabelNewMo, tvLabelFiler, dITittle, tvFromMt, tvToMt;
    private ImageView ivCourirPhoto;
    private EditText etFromTowerTransfer,etFromFloorTransfer,etFromAreaTransfer,
            etMaterialTransfer,etSenderTransfer,etToBeTransfer, etToTowerTransfer,
            etToFloorTransfer,etToAreaTransfer,etRecipientTransfer, dIComment, etComment;
    private static final int request_data_from  = 20;
    private static final int request_data_from_2  = 21;
    private static final int request_data_to_confirmation = 22;
    private String idtransfer, idmaterial, approvalnumber, statusrequest, kodetransfer, comment;
    private Button btnApproveTrans, btnRejectTrans, btnPostingTrans, btnConfirmation, btnRejectQspv, dIBtnCancel, dIBtnSubmit;
    private Dialog dialog, dialogInputText;
    private LinearLayout llPostingTrans, llApprovalTrans, llConfirmation, llComment;
    String today;
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);

    Calendar myCalendar = new GregorianCalendar();

    public ApprovalTransferMaterialActivity() {
        // Required empty public constructor
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approvaltransfer);

        controller = new MMController();
        session = new SessionManager(getApplicationContext());

        Intent intent = getIntent();
        Bundle bd = intent.getExtras();
        if(bd != null)
        {
            idtransfer = (String) bd.get("IdTransfer");
            idmaterial = (String) bd.get("IdMaterial");
            approvalnumber = (String) bd.get("ApprovalNumber");
            statusrequest = (String) bd.get("StatusRequest");
            kodetransfer = (String) bd.get("KodeTransfer");
            comment = (String) bd.get("Comment");
            Log.d("ASd", approvalnumber);

            //JIKA TIDAK ADA AKSES
            if(!session.getCanEdit().toString().equals("1"))
            {
                Toast.makeText(getApplicationContext(),"You dont have Privilege to Edit!", Toast.LENGTH_SHORT).show();
                this.finish();
            }
        }

        etFromTowerTransfer = (EditText) findViewById(R.id.etFromTowerTransfer);
        etFromFloorTransfer= (EditText) findViewById(R.id.etFromFloorTransfer);
        etFromAreaTransfer= (EditText) findViewById(R.id.etFromAreaTransfer);
        etMaterialTransfer= (EditText) findViewById(R.id.etMaterialTransfer);
        etSenderTransfer= (EditText) findViewById(R.id.etSenderTransfer);

        etToBeTransfer= (EditText) findViewById(R.id.etToBeTransfer);
        etToTowerTransfer= (EditText) findViewById(R.id.etToTowerTransfer);
        etToFloorTransfer= (EditText) findViewById(R.id.etToFloorTransfer);
        etToAreaTransfer= (EditText) findViewById(R.id.etToAreaTransfer);
        etRecipientTransfer= (EditText) findViewById(R.id.etRecipientTransfer);
        etComment = (EditText) findViewById(R.id.etComment);

        etFromTowerTransfer.setEnabled(false);
        etFromFloorTransfer.setEnabled(false);
        etFromAreaTransfer.setEnabled(false);
        etMaterialTransfer.setEnabled(false);
        etSenderTransfer.setEnabled(false);
        etToBeTransfer.setEnabled(false);
        etToTowerTransfer.setEnabled(false);
        etToFloorTransfer.setEnabled(false);
        etToAreaTransfer.setEnabled(false);
        etRecipientTransfer.setEnabled(false);

        btnApproveTrans = (Button) findViewById(R.id.btnApproveTrans);
        btnRejectTrans = (Button) findViewById(R.id.btnRejectTrans);
        btnPostingTrans = (Button) findViewById(R.id.btnPostingTrans);
        llPostingTrans = (LinearLayout) findViewById(R.id.llPostingTrans);
        llApprovalTrans = (LinearLayout) findViewById(R.id.llApprovalTrans);
        llConfirmation = (LinearLayout) findViewById(R.id.llConfirmation);
        llComment = (LinearLayout) findViewById(R.id.llComment);

        btnConfirmation = (Button) findViewById(R.id.btnConfirmation);
        btnRejectQspv = (Button) findViewById(R.id.btnRejectQspv);

        dialog = new Dialog(ApprovalTransferMaterialActivity.this);
        today = dateFormat.format(myCalendar.getTime());

        //Dialog untuk input text
        dialogInputText = new Dialog(ApprovalTransferMaterialActivity.this);
        dialogInputText.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogInputText.setContentView(R.layout.general_dialog_inputtext);
        dialogInputText.setCanceledOnTouchOutside(false);
        dITittle = (TextView) dialogInputText.findViewById(R.id.dITittle);
        dIComment = (EditText) dialogInputText.findViewById(R.id.dIComment);
        dIBtnCancel = (Button) dialogInputText.findViewById(R.id.dIBtnCancel);
        dIBtnSubmit = (Button) dialogInputText.findViewById(R.id.dIBtnSubmit);

        tvFromMt = (TextView) findViewById(R.id.tvFromMt);
        tvToMt = (TextView) findViewById(R.id.tvToMt);
        tvFromMt.setBackgroundResource(R.drawable.btn_bluetrans);
        tvToMt.setBackgroundResource(R.drawable.btn_bluetrans);

        getSupportActionBar().setTitle("Confirmation Transfer Material");
        getSupportActionBar().setSubtitle(kodetransfer);

        btnRejectTrans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dITittle.setText("Reject Reason");

                dIBtnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!dIComment.getText().toString().equals("")) {
                            //onApprove(kodespn, approvalnumber, "0", dIComment.getText().toString());
                            onApprove(kodetransfer, approvalnumber, "0", idtransfer, dIComment.getText().toString());
                        }
                        else
                        {
                            Toast toast = Toast.makeText(ApprovalTransferMaterialActivity.this,"Reason Required!", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            dIComment.setText("");
                            dIComment.requestFocus();
                        }
                    }
                });

                dIBtnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogInputText.dismiss();
                    }
                });

                dialogInputText.show();


            }
        });

        btnApproveTrans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onApprove(kodetransfer, approvalnumber, "1", idtransfer, "");
            }
        });

        btnPostingTrans.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onPosting(kodetransfer, idtransfer);
            }
        });

        btnConfirmation.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateStatusConfirmation(approvalnumber, idtransfer, kodetransfer, "4", "");
            }
        });

        btnRejectQspv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dITittle.setText("Reject Reason");

                dIBtnSubmit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if(!dIComment.getText().toString().equals("")) {
                            updateStatusConfirmation(approvalnumber, idtransfer, kodetransfer, "5", dIComment.getText().toString());
                        }
                        else
                        {
                            Toast toast = Toast.makeText(ApprovalTransferMaterialActivity.this,"Reason Required!", Toast.LENGTH_LONG);
                            toast.setGravity(Gravity.CENTER, 0, 0);
                            toast.show();
                            dIComment.setText("");
                            dIComment.requestFocus();
                        }
                    }
                });

                dIBtnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialogInputText.dismiss();
                    }
                });

                dialogInputText.show();


            }
        });
//
            SetListView(idtransfer);
//        SetReadyStockMaterial(idmaterial);
//        getIdleStockQty(idmaterial);
           CheckAction(statusrequest);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode,resultCode,data);


        if( requestCode == request_data_from ) {
            if (data != null){
                //projectSelected.setText(data.getExtras().getString("projectname"));
                session.setKodeProyek(data.getExtras().getString("kodeProyek"));
                session.setNamaProyek(data.getExtras().getString("projectname"));

                //Toast.makeText(getApplicationContext(), "Sampai sini Kode Proyek"+data.getExtras().getString("kodeProyek"), Toast.LENGTH_LONG).show();
                //Toast.makeText(getApplicationContext(), "Sampai sini Kode2 Proyek"+session.getKodeProyek(), Toast.LENGTH_LONG).show();

                //setupViewPager(viewPager);
            }
        }

    }

    public void CheckAction(String statusrequest)
    {
//        if(statusrequest.equals(""))
//        {
//
//        }
//        else if(statusrequest.equals("Waiting for Approval"))
//        {
//            IsCanApprove(kodetransfer, approvalnumber, idtransfer);
//        }
//        else if(statusrequest.equals("ON PROGRESS"))
//        {
//            IsCanApprove(kodetransfer, approvalnumber, idtransfer);
//        }


        if(statusrequest.equals("1") && approvalnumber.equals(""))
        {
            getSupportActionBar().setTitle("Post Transfer Material");
            isCanPosting(kodetransfer, idtransfer);
            Log.d("MTLog", "Not Posted");
        }
        else if(statusrequest.equals("1") && !approvalnumber.equals(""))
        {
            getSupportActionBar().setTitle("Approve Transfer Material");
            IsCanApprove(kodetransfer, approvalnumber, idtransfer);
            Log.d("MTLog", "Waiting");
        }
        else if(statusrequest.equals("2"))
        {
            getSupportActionBar().setTitle("Confirm Transfer Material");
            llConfirmation.setVisibility(View.VISIBLE);
            Log.d("MTLog", "Approved by SM");
        }
        else if(statusrequest.equals("3"))
        {
            getSupportActionBar().setTitle("Confirm Transfer Material");
            llComment.setVisibility(View.VISIBLE);
            etComment.setText(comment);
            Log.d("MTLog", "Reject by SM");
        }
        else if(statusrequest.equals("4"))
        {
            getSupportActionBar().setTitle("Information Transfer Material");
            Log.d("MTLog", "Confirmed by Receiver");
        }
        else if(statusrequest.equals("5"))
        {
            getSupportActionBar().setTitle("Information Transfer Material");
            llComment.setVisibility(View.VISIBLE);
            etComment.setText(comment);
            Log.d("MTLog", "Reject by Receiver");
        }


//        else if(statusrequest.equals("4"))
//        {
//            llConfirmation.setVisibility(View.VISIBLE);
//        }

    }

    private void isCanPosting(String kodematerialout, String IdMo) {

        MProgressDialog.showProgressDialog(ApprovalTransferMaterialActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();
        ApprovalEnt approvalEnt = new ApprovalEnt();
        approvalEnt.setNik(session.getKeyNik());
        approvalEnt.setKode_proyek(session.getKodeProyek());
        approvalEnt.setForm_id("MAMAN.02");
        approvalEnt.setReference_id(kodematerialout);
        approvalEnt.setMode("is_can_posting");
        approvalEnt.setIs_approve("");
        approvalEnt.setApproval_number("0");
        approvalEnt.setTokenID(session.getUserToken());

        String jsonString = gsonHeader.toJson(approvalEnt);

        controller.ApprovalConnectionString(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

                MProgressDialog.dismissProgressDialog();
                JsonArray jArray = new JsonParser().parse(output).getAsJsonArray();
                JsonObject jsonObject = jArray.get(0).getAsJsonObject();

                if(jsonObject.get("IsCanPosting").getAsString().equals("true"))
                {
                    llPostingTrans.setVisibility(View.VISIBLE);
                }
            }
        });
    }

    private void IsCanApprove(String kodematerialout, String ApprovalNumber, String IdMo) {

        MProgressDialog.showProgressDialog(ApprovalTransferMaterialActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();
        ApprovalEnt approvalEnt = new ApprovalEnt();
        approvalEnt.setNik(session.getKeyNik());
        approvalEnt.setKode_proyek(session.getKodeProyek());
        approvalEnt.setForm_id("MAMAN.02");
        approvalEnt.setReference_id(kodematerialout);
        approvalEnt.setMode("");
        //approvalEnt.setIs_approve("");
        approvalEnt.setApproval_number(ApprovalNumber);
        approvalEnt.setTokenID(session.getUserToken());

        String jsonString = gsonHeader.toJson(approvalEnt);

        controller.ApprovalConnectionString(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {
                Log.d("Saapp", output);
                MProgressDialog.dismissProgressDialog();
                JsonArray jArray = new JsonParser().parse(output).getAsJsonArray();
                JsonObject jsonObject = jArray.get(0).getAsJsonObject();

                if(jsonObject.get("IsCanApprove").getAsString().equals("true"))
                {
                    llApprovalTrans.setVisibility(View.VISIBLE);
                }
                else
                {
                    Toast.makeText(getApplicationContext(), jsonObject.get("Message").getAsString()+"!", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    private void onApprove(String kodematerialout, String ApprovalNumber, String IsApprove, String idtransfer, String Reason) {

        MProgressDialog.showProgressDialog(ApprovalTransferMaterialActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();
        ApprovalEnt approvalEnt = new ApprovalEnt();
        approvalEnt.setNik(session.getKeyNik());
        approvalEnt.setKode_proyek(session.getKodeProyek());
        approvalEnt.setForm_id("MAMAN.02");
        approvalEnt.setReference_id(kodematerialout);
        approvalEnt.setMode("");
        approvalEnt.setIs_approve(IsApprove);
        approvalEnt.setApproval_number(ApprovalNumber);
        approvalEnt.setTokenID(session.getUserToken());
        approvalEnt.setReason(Reason);

        String jsonString = gsonHeader.toJson(approvalEnt);

        controller.ApprovalConnectionString(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {
                updateStatusRequest(ApprovalNumber, idtransfer, kodematerialout, IsApprove, Reason);
            }
        });
    }

    private void onPosting(String kodematerialout, String IdMo) {

        MProgressDialog.showProgressDialog(ApprovalTransferMaterialActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();
        ApprovalEnt approvalEnt = new ApprovalEnt();
        approvalEnt.setNik(session.getKeyNik());
        approvalEnt.setKode_proyek(session.getKodeProyek());
        approvalEnt.setForm_id("MAMAN.02");
        approvalEnt.setReference_id(kodematerialout);
        approvalEnt.setMode("posting");
        approvalEnt.setIs_approve("");
        approvalEnt.setApproval_number("0");
        approvalEnt.setTokenID(session.getUserToken());

        String jsonString = gsonHeader.toJson(approvalEnt);

        controller.ApprovalConnectionString(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {


                Log.d("OmOm2", output);
                MProgressDialog.dismissProgressDialog();
                JsonArray jArray = new JsonParser().parse(output).getAsJsonArray();
                JsonObject jsonObject = jArray.get(0).getAsJsonObject();

//                if(jsonObject.get("IsCanApprove").getAsString().equals("true"))
//                {
                    Log.d("OmOm3", output);
                    updateRequestMO(jsonObject.get("Approval_Number").getAsString(), IdMo, kodematerialout);
//                }

            }
        });
    }

    private void updateRequestMO(String ApprovalNumber, String IdMo, String KodeMaterialOut) {
        Log.d("OmOm4", ApprovalNumber);
//        MProgressDialog.showProgressDialog(ApprovalTransferMaterialActivity.this, true, new DialogInterface.OnCancelListener() {
//            @Override
//            public void onCancel(DialogInterface dialog) {
//                MProgressDialog.dismissProgressDialog();
//            }
//        });

        Gson gsonHeader = new Gson();
        ListItemTransferOutFullTableEnt commonRequestMoFormEnt = new ListItemTransferOutFullTableEnt();
        commonRequestMoFormEnt.setID(IdMo);
        commonRequestMoFormEnt.setKode_Transfer(KodeMaterialOut);
        commonRequestMoFormEnt.setKode_Proyek(session.getKodeProyek());
        commonRequestMoFormEnt.setKode_Item("1");
        commonRequestMoFormEnt.setVolume("1");
        commonRequestMoFormEnt.setUnit("unit");
        //a = initial
        commonRequestMoFormEnt.setFromQSPV("a");
        commonRequestMoFormEnt.setFromKode_Tower("a");
        commonRequestMoFormEnt.setFromKode_Lantai("a");
        commonRequestMoFormEnt.setFromKode_Zona("0");

        commonRequestMoFormEnt.setToKode_Tower("a");
        commonRequestMoFormEnt.setToKode_Lantai("a");
        commonRequestMoFormEnt.setToKode_Zona("0");
        commonRequestMoFormEnt.setFlagReturnGudang("0");
        commonRequestMoFormEnt.setToQSPV("a");

        commonRequestMoFormEnt.setCourierPhoto("a");
        commonRequestMoFormEnt.setStatusRequest("1");
        commonRequestMoFormEnt.setApprovalNo(ApprovalNumber);
        commonRequestMoFormEnt.setStatusAktif("True");
        commonRequestMoFormEnt.setPembuat(session.getKeyNik());
        commonRequestMoFormEnt.setWaktuBuat(today);
        commonRequestMoFormEnt.setPengubah(session.getKeyNik());
        commonRequestMoFormEnt.setWaktuUbah(today);
        commonRequestMoFormEnt.setComment("0");

        commonRequestMoFormEnt.setTokenID(session.getUserToken());
//        commonRequestMoFormEnt.setModeReq("POSTING");
        commonRequestMoFormEnt.setFormId("MAMAN.02");

        String jsonString = gsonHeader.toJson(commonRequestMoFormEnt);
        Log.d("OmOm5", ApprovalNumber);
        controller.SaveGeneralObjectTransferMo(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {
                Log.d("OmOm7", output);
                Toast toast = Toast.makeText(ApprovalTransferMaterialActivity.this,KodeMaterialOut+" Posting Success !", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                Intent intent = new Intent();
                intent.putExtra("", "");
                setResult(request_data_to_confirmation, intent);
                finish();

            }
        });
    }

    private void updateStatusRequest(String ApprovalNumber, String IdMo, String KodeMaterialOut, String IsApprove, String reason) {
        Log.d("OmOm4", ApprovalNumber);
        MProgressDialog.showProgressDialog(ApprovalTransferMaterialActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();

        ListItemTransferOutFullTableEnt commonRequestMoFormEnt = new ListItemTransferOutFullTableEnt();
        commonRequestMoFormEnt.setID(IdMo);
        commonRequestMoFormEnt.setKode_Transfer(KodeMaterialOut);
        commonRequestMoFormEnt.setKode_Proyek(session.getKodeProyek());
        commonRequestMoFormEnt.setKode_Item("1");
        commonRequestMoFormEnt.setVolume("1");
        commonRequestMoFormEnt.setUnit("unit");
        //a = initial
        commonRequestMoFormEnt.setFromQSPV("a");
        commonRequestMoFormEnt.setFromKode_Tower("a");
        commonRequestMoFormEnt.setFromKode_Lantai("a");
        commonRequestMoFormEnt.setFromKode_Zona("0");

        commonRequestMoFormEnt.setToKode_Tower("a");
        commonRequestMoFormEnt.setToKode_Lantai("a");
        commonRequestMoFormEnt.setToKode_Zona("0");
        commonRequestMoFormEnt.setFlagReturnGudang("0");
        commonRequestMoFormEnt.setToQSPV("a");

        commonRequestMoFormEnt.setCourierPhoto("a");
        if(IsApprove.equals("1")) {
            commonRequestMoFormEnt.setStatusRequest("2");
        }
        else
        {
            commonRequestMoFormEnt.setStatusRequest("3");
        }
        commonRequestMoFormEnt.setApprovalNo(ApprovalNumber);
        commonRequestMoFormEnt.setStatusAktif("True");
        commonRequestMoFormEnt.setPembuat(session.getKeyNik());
        commonRequestMoFormEnt.setWaktuBuat(today);
        commonRequestMoFormEnt.setPengubah(session.getKeyNik());
        commonRequestMoFormEnt.setWaktuUbah(today);
        commonRequestMoFormEnt.setComment(reason);

        commonRequestMoFormEnt.setTokenID(session.getUserToken());
//        commonRequestMoFormEnt.setModeReq("POSTING");
        commonRequestMoFormEnt.setFormId("MAMAN.02");


//
        String jsonString = gsonHeader.toJson(commonRequestMoFormEnt);
        Log.d("OmOm5", ApprovalNumber);
        controller.SaveGeneralObjectTransferMo(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

                Toast toast = Toast.makeText(ApprovalTransferMaterialActivity.this,KodeMaterialOut+" Approve Success !", Toast.LENGTH_LONG);
                toast.setGravity(Gravity.CENTER, 0, 0);
                toast.show();

                Intent intent = new Intent();
                intent.putExtra("", "");
                setResult(request_data_to_confirmation, intent);
                finish();

            }
        });
    }

    private void updateStatusConfirmation(String ApprovalNumber, String IdMo, String KodeMaterialOut, String statusconfirm, String reason) {
        Log.d("OmOm4", ApprovalNumber);
        MProgressDialog.showProgressDialog(ApprovalTransferMaterialActivity.this, true, new DialogInterface.OnCancelListener() {
            @Override
            public void onCancel(DialogInterface dialog) {
                MProgressDialog.dismissProgressDialog();
            }
        });

        Gson gsonHeader = new Gson();

        ListItemTransferOutFullTableEnt commonRequestMoFormEnt = new ListItemTransferOutFullTableEnt();
        commonRequestMoFormEnt.setID(IdMo);
        commonRequestMoFormEnt.setKode_Transfer(KodeMaterialOut);
        commonRequestMoFormEnt.setKode_Proyek(session.getKodeProyek());
        commonRequestMoFormEnt.setKode_Item("1");
        commonRequestMoFormEnt.setVolume("1");
        commonRequestMoFormEnt.setUnit("unit");
        //a = initial
        commonRequestMoFormEnt.setFromQSPV("a");
        commonRequestMoFormEnt.setFromKode_Tower("a");
        commonRequestMoFormEnt.setFromKode_Lantai("a");
        commonRequestMoFormEnt.setFromKode_Zona("0");

        commonRequestMoFormEnt.setToKode_Tower("a");
        commonRequestMoFormEnt.setToKode_Lantai("a");
        commonRequestMoFormEnt.setToKode_Zona("0");
        commonRequestMoFormEnt.setFlagReturnGudang("0");
        commonRequestMoFormEnt.setToQSPV("a");
        commonRequestMoFormEnt.setCourierPhoto("a");
        commonRequestMoFormEnt.setStatusRequest(statusconfirm);
        commonRequestMoFormEnt.setApprovalNo(ApprovalNumber);
        commonRequestMoFormEnt.setStatusAktif("True");
        commonRequestMoFormEnt.setPembuat(session.getKeyNik());
        commonRequestMoFormEnt.setWaktuBuat(today);
        commonRequestMoFormEnt.setPengubah(session.getKeyNik());
        commonRequestMoFormEnt.setWaktuUbah(today);
        commonRequestMoFormEnt.setComment(reason);

        commonRequestMoFormEnt.setTokenID(session.getUserToken());
//        commonRequestMoFormEnt.setModeReq("POSTING");
        commonRequestMoFormEnt.setFormId("MAMAN.02");
//
        String jsonString = gsonHeader.toJson(commonRequestMoFormEnt);
        Log.d("OmOm5", ApprovalNumber);
        controller.SaveGeneralObjectTransferMo(getApplicationContext(),jsonString,new VolleyCallback(){
            @Override
            public void onSuccess(JSONArray result) {
                // MProgressDialog.dismissProgressDialog();
            }

            @Override
            public void onSave(String output) {

                if(statusconfirm.equals("4")) {
                    Toast toast = Toast.makeText(ApprovalTransferMaterialActivity.this, KodeMaterialOut + " Confirmed Received!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }
                else
                {
                    Toast toast = Toast.makeText(ApprovalTransferMaterialActivity.this, KodeMaterialOut + " Confirmed Rejected!", Toast.LENGTH_LONG);
                    toast.setGravity(Gravity.CENTER, 0, 0);
                    toast.show();
                }

                Intent intent = new Intent();
                intent.putExtra("", "");
                setResult(request_data_to_confirmation, intent);
                finish();

            }
        });
    }

    public void updateStatusRequest(String action)
    {
        Gson gsonHeader = new Gson();
        checkApprovalEnt checkapprovalEnt = new checkApprovalEnt();
        checkapprovalEnt.setUserid(session.isNikUserLoggedIn());
        checkapprovalEnt.setKodeProyek(session.getKodeProyek());
        checkapprovalEnt.setAppprovalNo(approvalnumber);

        String jsonStringCheck = gsonHeader.toJson(checkapprovalEnt);

        if(action.equals("Approve"))
        {
            controller.SaveGeneralObjectPostApproval(getApplicationContext(), jsonStringCheck, new VolleyCallback() {
                @Override
                public void onSuccess(JSONArray result) {
                    //Toast.makeText(getApplicationContext(),"CheckApproval result "+result,Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSave(String output) {

                    Bundle sendBundle = new Bundle();
                    sendBundle.putString("data", "");
                    Intent intent = new Intent(getApplicationContext(), RequestMaterialActivity.class );
                    intent.putExtras(sendBundle);
                    setResult(22,intent);
                    finish();

                    Toast.makeText(getApplicationContext(), "Submit Approval Success !" + output, Toast.LENGTH_SHORT).show();
                    //Toast.makeText(getApplicationContext(),"CheckApproval "+output,Toast.LENGTH_SHORT).show();
                    //Intent intent = new Intent(ListPOActivity.this, CartQtyReceivedActivity.class);
                    //startActivity(intent);
                }
            });
        }
        else
        {
            controller.SaveGeneralObjectPostReject(getApplicationContext(), jsonStringCheck, new VolleyCallback() {
                @Override
                public void onSuccess(JSONArray result) {
                    //Toast.makeText(getApplicationContext(),"CheckApproval result "+result,Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onSave(String output) {

                    Bundle sendBundle = new Bundle();
                    sendBundle.putString("data", "");
                    Intent intent = new Intent(getApplicationContext(), RequestMaterialActivity.class );
                    intent.putExtras(sendBundle);
                    setResult(22,intent);
                    finish();
                    Toast.makeText(getApplicationContext(), "Submit Reject Success !" + output, Toast.LENGTH_SHORT).show();
                    //Toast.makeText(getApplicationContext(),"CheckApproval "+output,Toast.LENGTH_SHORT).show();
                    //Intent intent = new Intent(ListPOActivity.this, CartQtyReceivedActivity.class);
                    //startActivity(intent);
                }
            });
        }
    }

    public void SetListView(String wherecond){

        session = new SessionManager(getApplicationContext());
        //listItems.clear();
        String whereCond = "";
        if (!wherecond.equals("")){
            whereCond = wherecond;
        }
        controller.InqGeneral2(getApplicationContext(),"SPM_GetListTransferMaterialOut_Param",
                "@KodeProject"," '"+session.getKodeProyek()+"' ",
                "@currentpage","1",
                "@pagesize","10",
                "@sortby","",
                "@wherecond"," AND TTrans.ID ="+wherecond,
                "@nik"," '"+session.getKeyNik()+"' ",
                "@formid","MAMAN.02",
                "@zonaid","",
                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            Log.d("EZRAsss", result.toString());

                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    item = result.getJSONObject(i);

                                    etFromTowerTransfer.setText(item.getString("Nama_Tower"));
                                    etFromFloorTransfer.setText(item.getString("Nama_Lantai"));
                                    etFromAreaTransfer.setText(item.getString("Nama_Zona"));
                                    etMaterialTransfer.setText(item.getString("Deskripsi"));
                                    etSenderTransfer.setText(item.getString("NamaSender"));


                                    etToBeTransfer.setText(item.getString("Volume"));
                                    etToTowerTransfer.setText(item.getString("Nama_KeTower"));
                                    etToFloorTransfer.setText(item.getString("Nama_KeLantai"));
                                    etToAreaTransfer.setText(item.getString("Nama_KeZona"));
                                    etRecipientTransfer.setText(item.getString("NamaReceiver"));


//                                    ListItemTransferOut listProjectEnt = new ListItemTransferOut(
//                                            item.getString("ID"), item.getString("Kode_Transfer"),
//                                            item.getString("Kode_Proyek"), item.getString("Kode_Item"),
//                                            item.getString("Volume"),item.getString("Unit"),
//                                            item.getString("FromQSPV"),item.getString("FromKode_Tower"),
//                                            item.getString("FromKode_Lantai"),item.getString("FromKode_Zona"),
//                                            item.getString("ToQSPV"),item.getString("ToKode_Tower"),
//                                            item.getString("ToKode_Lantai"),item.getString("ToKode_Zona"),
//                                            item.getString("CourierPhoto"),item.getString("StatusRequest"),
//                                            item.getString("ApprovalNo"),item.getString("StatusAktif"),
//                                            item.getString("Pembuat"),item.getString("WaktuBuat"),
//                                            item.getString("Pengubah"),item.getString("WaktuUbah"),
//                                            item.getString("Comment"),item.getString("FlagReturnGudang"),
//                                            item.getString("Deskripsi"),item.getString("Nama_Tower"),
//                                            item.getString("Nama_Lantai"),item.getString("Nama_Zona")
//                                    );

                                    //listItems.add(listProjectEnt);
                                }

                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public void SetReadyStockMaterial(String wherecond){
        session = new SessionManager(getApplicationContext());
        //listItems.clear();

        controller.InqGeneral3(getApplicationContext(),"SPM_GetQtyReadyStockFromViewParam",
                "@KodeProject"," '"+session.getKodeProyek()+"' ",
                "@currentpage","1",
                "@pagesize","10",
                "@sortby","",
                "@wherecond"," WHERE Kode_Proyek = '"+session.getKodeProyek()+ "' AND KatalogID = "+wherecond,


                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            Log.d("WOI2",result.toString());
                            if (result.length() > 0 || !result.toString().equals("[]")) {
                                for (int i = 0; i < result.length(); i++) {
                                    item = result.getJSONObject(i);
//                                    etReadyStock.setText(item.getString("LatestVol"));
                                }
                            }
                            else {
//                                etReadyStock.setText("0");
                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    public void getIdleStockQty(String material){
        Log.d("material", material);
        session = new SessionManager(getApplicationContext());
        controller.InqGeneral3(getApplicationContext(),"SPM_GetQtyIdleStockFromView_Param",
                "@KodeProject"," '"+session.getKodeProyek()+"' ",
                "@currentpage","1",
                "@pagesize","10",
                "@sortby","",
                "@wherecond"," WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND Kode_Item = '"+material+"' AND FlagReturnGudang = 1 ",
                //"@wherecond"," WHERE Kode_Proyek = '"+session.getKodeProyek()+"' AND Kode_Item = '"+material+"' AND ToKode_Tower = '"+tower+"' AND FlagReturnGudang = 1 ",
                //"@nik",String.valueOf(session.isLoggedIn()),
                //"@formid","MAMAN.02",
                //"@zonaid","",
                new VolleyCallback() {
                    @Override
                    public void onSave(String output) {

                    }

                    @Override
                    public void onSuccess(JSONArray result) {
                        try {
                            Log.d("EZRA", result.toString());
                            if (result.length() > 0) {
                                for (int i = 0; i < result.length(); i++) {
                                    item = result.getJSONObject(i);
//                                    etIdleStock.setText(item.getString("vol"));
                                }

                            }
                            else
                            {
//                                etIdleStock.setText("0");
                            }
                        }catch (JSONException e){
                            Toast.makeText(getApplicationContext(), "CATCH ", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    @Override
    public void onBackPressed() {
        this.finish();
    }

    public boolean isNetworkAvailable() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = manager.getActiveNetworkInfo();
        boolean isAvailable = false;
        if (networkInfo != null && networkInfo.isConnected()) {
            // Network is present and connected
            isAvailable = true;
            //btnConnect.setVisibility(View.GONE);
            //h2.removeCallbacks(run);
        }
        else
        {
            isAvailable = false;
            ///h2.postDelayed(run, 0);
            //btnConnect.setVisibility(View.VISIBLE);
        }
        return isAvailable;
    }



    public void animateFAB(){

        if(isFabOpen){

            fab.startAnimation(rotate_backward);
            fab1.startAnimation(fab_close);
            fab2.startAnimation(fab_close);
            tvLabelNewMo.startAnimation(fab_close);
            tvLabelFiler.startAnimation(fab_close);
            fab1.setClickable(false);
            fab2.setClickable(false);
            isFabOpen = false;
            Log.d("Raj", "close");

        } else {

            fab.startAnimation(rotate_forward);
            fab1.startAnimation(fab_open);
            fab2.startAnimation(fab_open);
            tvLabelNewMo.startAnimation(fab_open);
            tvLabelFiler.startAnimation(fab_open);
            fab1.setClickable(true);
            fab2.setClickable(true);
            isFabOpen = true;
            Log.d("Raj","open");

        }
    }


}
